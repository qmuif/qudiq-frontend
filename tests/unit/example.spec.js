import { shallowMount } from '@vue/test-utils'

// Небольшой пример юнит теста
describe('ExampleComponent.vue', () => {
  it('renders props.msg when passed', () => {
    // немного сообщения
    // если оно найдено, все работает прекрасно
    expect('1').toMatch('1')
  })
})
