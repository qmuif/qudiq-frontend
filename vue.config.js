module.exports = {
  configureWebpack: {
    performance: {
      hints: false
    },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 244000
      }
    }
  },

  pwa: {
    name: 'BotMaker',
    msTileColor: '#1976D2'
  },

  devServer: {
    disableHostCheck: true
  },

  publicPath: undefined,
  outputDir: undefined,
  assetsDir: undefined,
  runtimeCompiler: undefined,
  productionSourceMap: false,
  parallel: undefined
}
