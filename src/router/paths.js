/**
 * Все маршруты приложения определяются тут
 * для дополнительной информации обратитесь к документации
 * https://router.vuejs.org/en/
 *
 * Логика роутинга в router/index.js
 */

export default [
  {
    path: '/',
    name: 'Главная',
    view: 'Main',
    meta: { locale: 'MainPage'}
  },
  {
    path: '/login',
    name: 'Авторизация',
    view: 'Login',
    meta: { locale: 'loginPage', shader: true, guest: true }
  },
  {
    path: '/registration',
    name: 'Регистрация',
    view: 'Register',
    meta: { locale: 'registration', shader: true, guest: true }
  },
  {
    path: '/verify-email/:email_key',
    name: 'Подтверждение почты',
    view: 'VerifyEmail',
    props: (route) => ({email_key: route.params.email_key}),
    meta: { locale: 'registration', shader: true, guest: true }
  },
  {
    path: '/bots',
    name: 'Боты',
    view: 'Bots/Main',
    meta: { locale: 'myBots'}
  },
  {
    path: '/bots/add',
    name: 'Добавить бота',
    view: 'Bots/Add',
    meta: { locale: 'myBots'}
  },
  {
    path: '/bots/:bot_id/responses',
    name: 'Настройка ответов',
    view: 'Bots/Responses',
    props: (route) => ({bot_id: route.params.bot_id}),
    meta: { locale: 'Ответы бота'}
  },
  {
    path: '/edit',
    name: 'Редактирование',
    view: 'ResponseProcess',
    meta: { locale: 'ResponseProcess'}
  }
]
