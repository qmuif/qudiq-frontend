/**
 * Vue Router
 *
 * Логика переадрессации и
 * Семантических(ЧПУ) URL
 *
 * @library
 *
 * https://router.vuejs.org/ru/
 */
// Зависимости библиотеки
import Vue from 'vue'
//import VueAnalytics from 'vue-analytics'
import Router from 'vue-router'
import Meta from 'vue-meta'
import store from '../store'
// Роуты
import paths from './paths'
function route (path, view, props, name, meta) {
  return {
    name: name || view,
    path,
    props,
    component: (resovle) => import(
      `@/views/${view}.vue`
    ).then(resovle),
    meta,
  }
}

/**
 * Логика роутинга
 * @type {VueRouter}
 *
 * Данные из 'path' проходят через функцию route и
 * создаёт новый массив с импортированными компонентами
 * @see route
 *
 * И добавяется условие переадрессации незнакомых url на главную
 */
const router = new Router({
  routes: paths.map(path => route(path.path, path.view, path.props, path.name, path.meta)).concat([
    { path: '*', redirect: '/' }
  ])
})
Vue.use(Router)
Vue.use(Meta)
// Начальная загрузка Analytics
// Дополнительная информация - https://github.com/MatteoGabriele/vue-analytics
// if (process.env.GOOGLE_ANALYTICS) {
//   Vue.use(VueAnalytics, {
//     id: process.env.GOOGLE_ANALYTICS,
//     router,
//     autoTracking: {
//       page: process.env.NODE_ENV !== 'development'
//     }
//   })
// }
/**
 * Логика проверки авторизации пользователя
 */
router.beforeEach((to, from, next) => {
  // если пользовател не авторизован и направляется не на '/login', перекидываем его на логин
  console.log(!!to.meta.guest)
  if (!store.getters.isAuthenticated && !to.meta.guest) {
    next('/login')
    // иначе, если авторизован и направляется на '/login', перекидываем его на главную
  } else if (store.getters.isAuthenticated && (to.path === '/login')) {
    next('/main')
    // иначе пусть идет куда хотел
  } else {
    next()
  }
})
export default router
