/**
 * Сборщик компонентов
 * Для использования компонента необходимо использовать его путь перед названием,
 * например :
 *  компонент находится по пути /components/test/MainComponent, тогда
 *  необходимо в коде прописать <test-main-component />
 */

import Vue from 'vue'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

const requireComponent = require.context(
  '@/components', true, /\.vue$/
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\//, '').replace(/\.\w+$/, ''))
  )

  Vue.component(componentName, componentConfig.default || componentConfig)
})
