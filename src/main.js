import Vue from 'vue'
import './registerServiceWorker'
import './plugins'
import './components'
import { sync } from 'vuex-router-sync'
import App from './App'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import i18n from './translate'
sync(store, router)
Vue.config.productionTip = false
new Vue({
  i18n,
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
