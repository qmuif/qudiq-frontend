// https://vuex.vuejs.org/en/getters.html

export default {
  isAuthenticated: state => !!state.token,
  getToken: state => state.token
}
