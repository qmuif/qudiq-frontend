/**
 * Vuex
 *
 * Хранилище
 *
 * Для использования локального хранилища используется vuex-persistedstate
 *
 * @library
 *
 * https://vuex.vuejs.org/en/
 */

import Vue from 'vue'
import Vuex from 'vuex'
// превращаем vuex в локальное хранилище
import createPersistedState from 'vuex-persistedstate'
// Модули
import modules from './modules'
// Экшены
import actions from './actions'
// Геттеры
import getters from './getters'
// Мутации
import mutations from './mutations'
// Хранилище
import state from './state'

/* Разбитые компоненты хранилища */

Vue.use(Vuex)

// Создание хранилища
const store = new Vuex.Store({
  actions,
  modules,
  plugins: [createPersistedState()],
  getters,
  mutations,
  state
})

export default store
