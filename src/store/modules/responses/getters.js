// https://vuex.vuejs.org/en/getters.html

export default {
  getBotId: state => state.botId,
  getResponses: state => state.responses,
  getModalIncomingMessage: state => state.modalIncomingMessage,
  getModalFileMessage: state => state.modalFileMessage,
  getModalDelete: state => state.modalDelete,
  getModalResponseMessage: state => state.modalResponseMessage,
}
