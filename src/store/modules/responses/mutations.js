import { set } from '@/utils/vuex'


function addModal(state, type) {
  state[type].show = true
  state[type].type = "add"
}
function editModal(state, type, data) {
  state[type].show = true
  state[type].type = "edit"
  state[type].id = data.id
  state[type].text = data.text
}
function closeModal(state, type) {
  state[type].show = false
  state[type].text = null
  state[type].id = null
}
function showDeleteModal(state, type, id, description) {
  state.modalDelete.show = true
  state.modalDelete.type = type
  state.modalDelete.id = id
  state.modalDelete.description = description
}

export default {
  setBotId: set('botId'),
  setResponses: set('responses'),
  setModalIncomingMessage: set('modalIncomingMessage'),
  addIncomingMessage(state, parent_id) {
    addModal(state, "modalIncomingMessage")
    state.modalIncomingMessage.parentId = parent_id
  },
  addResponseModal(state, id) {
    addModal(state, "modalResponseMessage")
    state.modalResponseMessage.bot_response = id
  },
  addFileModal(state, id) {
    addModal(state, "modalFileMessage")
    state.modalFileMessage.bot_response = id
  },
  showIncomeDeleteModal(state, id) {
    showDeleteModal(state, "bot-response", id, "Все вложенные ответы и обработчики так же будут удаленны!")
  },
  showResponseDeleteModal(state, id) {
    showDeleteModal(state, "response-message", id, "Бот перестанет отвечать данным сообщением!")
  },
  showFileDeleteModal(state, id) {
    showDeleteModal(state, "response-photo", id, "Бот перестанет отвечать данным сообщением!")
  },
  editIncomeMessage(state, data){
    editModal(state,"modalIncomingMessage", data)
  },
  editResponseModal(state, data){
    editModal(state,"modalResponseMessage", data)
  },
  editFileModal(state, data){
    editModal(state,"modalFileMessage", data)
    state.modalFileMessage.photo = data.photo
  },
  closeIncomeModal(state) {
    closeModal(state, "modalIncomingMessage")
  },
  closeResponseModal(state) {
    closeModal(state, "modalResponseMessage")
  },
  closeFileModal(state) {
    closeModal(state, "modalFileMessage")
    state.modalFileMessage.photo = null
  },
  closeDeleteModal(state) {
    state.modalDelete.show = false
    state.modalDelete.type = null
    state.modalDelete.id = null
    state.description = null
  },
}
