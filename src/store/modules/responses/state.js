export default {
  botId: null,
  modalIncomingMessage: {
    id: null,
    show: false,
    operationType: null,
    messageId: null,
    parentId: null,
    text: null
  },
  modalResponseMessage: {
    id: null,
    show: false,
    operationType: null,
    messageId: null,
    text: null,
    parent_id: null
  },
  modalFileMessage: {
    id: null,
    show: false,
    operationType: null,
    messageId: null,
  },
  modalDelete: {
    show: false,
    type: null,
    id: null,
    description: null
  },
  responses: [],
}
