/**
 * Сборщик модулей
 *
 * Vuex позволяет разделять хранилище на модули.
 * Каждый модуль может содержать собственное состояние, мутации, действия,
 * геттеры и даже встроенные подмодули
 *
 *
 * Подробная информация - https://vuex.vuejs.org/ru/guide/modules.html
 */

const requireModule = require.context('.', true, /\.js$/)
const modules = {}

requireModule.keys().forEach(fileName => {
  if (fileName === './index.js') return

  // Заменяем ./ и .js
  const path = fileName.replace(/(\.\/|\.js)/g, '')
  const [moduleName, imported] = path.split('/')

  if (!modules[moduleName]) {
    modules[moduleName] = {
      namespaced: true
    }
  }

  modules[moduleName][imported] = requireModule(fileName).default
})

export default modules
