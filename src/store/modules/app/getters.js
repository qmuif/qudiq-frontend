// https://vuex.vuejs.org/en/getters.html

export default {
  getLocale: state => state.locale,
  isDarkTheme: state => !!state.darkTheme,
  getDrawer: state => state.drawer,
  getError: state => state.showError,
  getErrorText: state => state.errorText
}
