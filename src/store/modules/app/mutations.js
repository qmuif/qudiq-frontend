import { set, toggle } from '@/utils/vuex'

export default {
  setLocale: set('locale'),
  setDarkTheme: toggle('darkTheme'),
  setDrawer: set('drawer'),
  toggleDrawer: toggle('drawer'),
  setError: set('showError'),
  setErrorText: set('errorText')
}
