export default {
  locale: 'ru',
  darkTheme: false,
  drawer: false,
  showError: false,
  errorText: ''
}
