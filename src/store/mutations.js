// https://vuex.vuejs.org/en/mutations.html
export default {
  /**
   * Устанавливаем токен в локальное хранилище
   */
  setToken (state, token) {
    state.token = token
  },
  deleteToken(state){
    state.token = null;
  },
}
