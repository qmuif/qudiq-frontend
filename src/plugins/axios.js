import Vue from 'vue'
// Lib imports
import axios from 'axios'
import store from '../store'
import router from "../router";


function errorResponseHandler(error) {
  if (error.response) {
    if(error.response.status === 401){
      store.commit('deleteToken');
      router.push({ path: '/login' })
    }
  }
  return Promise.reject(error);
}

axios.interceptors.response.use(
  response => response,
  errorResponseHandler
);


Vue.prototype.$http = axios;
Vue.prototype.$http.defaults.baseURL =
  process.env.NODE_ENV === 'production' // если сборка для продакшена
    ? 'http://5.253.63.186:9000/' // url обычный
    : 'http://127.0.0.1:8000/'; // url локальный
// если есть токен, ставим его в headers
if (store.getters.isAuthenticated) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = `Bearer ${store.getters.getToken}`;
}
