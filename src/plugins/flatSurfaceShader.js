/**
 * Шейдер фона
 * подробнее https://grzhan.github.io/vue-flat-surface-shader/?ref=madewithvuejs.com
 */

import FlatSurfaceShader from 'vue-flat-surface-shader'
import Vue from 'vue'

Vue.use(FlatSurfaceShader)
