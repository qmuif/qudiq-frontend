import Vue from 'vue'
import VueI18n from 'vue-i18n'
import AppEn from './translate/en'
import AppRu from './translate/ru'

Vue.use(VueI18n)
// Ready translated locale messages
const messages = {
  en: AppEn,
  ru: AppRu
}

export default new VueI18n({
  locale: 'ru', // устанавливаем язык по умолчанию
  messages // переводы
})
