export default {
  loginPage: 'Авторизация',
  loginFrom: 'Авторизация',
  login: 'Логин',

  password: 'Пароль',
  entrance: 'Вход',

  email: 'Почта',
  emailFailed: 'Неверный формат',

  registration: 'Регистрация',
  registrationForm: 'Регистрация',
  toRegister: 'Готово',
  back: 'Назад',

  MainPage: 'Главная',
  ResponseProcess: 'Ответы',
  statistics: 'Статистика',
  tasks: 'Задачи',
  monitoring: 'Мониторинг',
  bot: 'Бот',
  incomingMessages: 'Входящие сообщения',
  unrecognizedMessages: 'Неопознано сообщений',
  replies: 'Ответов',
  userRating: 'Оценка пользователей',
  frequencyOfCallsPerDay: 'Частота обращений за сутки',
  productivity: 'Продуктивность',
  botsInfo: 'Информация по вашим ботам',
  search: 'Поиск',
  export: 'Экспортировать',

  myBots: 'Мои боты',

  settings: 'Настройки',

  AboutPage: 'О нас',

  exit: 'Выход',

  fieldRequired: 'Это поле обязательно для заполнения',

  doYouWantToGetRidOfRestrictions: 'Хотите избавиться от ограничений?',
  subscribe: 'Оформить подписку',
  andGetRecommendationsForImprovingBots: 'И получать рекомендации по улучшению ботов?',

  add: "Добавить",
  edit: "Изменить",
}
