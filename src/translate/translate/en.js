export default {
  loginPage: 'Login',
  loginFrom: 'Login Form',
  login: 'Login',

  password: 'Password',
  entrance: 'Login',

  email: 'E-mail',
  emailFailed: 'Wrong format',

  registration: 'Registration',
  registrationForm: 'Registration Form',
  toRegister: 'register',
  back: 'Back',

  MainPage: 'Main',
  ResponseProcess: 'Answers',
  statistics: 'Statistics',
  tasks: 'Tasks',
  monitoring: 'Monitoring',
  bot: 'Bot',
  incomingMessages: 'Incoming Messages',
  unrecognizedMessages: 'Unrecognized Messages',
  replies: 'Replies',
  userRating: 'User rating',
  frequencyOfCallsPerDay: 'Frequency of calls per day',
  productivity: 'Productivity',
  botsInfo: 'Your bot information',
  search: 'Search',
  export: 'Export',

  myBots: 'My bots',

  settings: 'Settings',

  AboutPage: 'About us',

  exit: 'Exit',

  fieldRequired: 'This field is required',

  doYouWantToGetRidOfRestrictions: 'Do you want to get rid of restrictions?',
  subscribe: 'Subscribe',
  andGetRecommendationsForImprovingBots: 'And get recommendations for improving bots?',

  add: "Add",
  edit: "Изменить",
}
