module.exports = {
  root: true,

  env: {
    node: true,
    es6: true
  },

  'extends': [
    'plugin:vue/recommended',
    'eslint:recommended'
  ],

  'plugins': [
    'vue',
  ],

  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'no-param-reassign': 'warn',
    "indent": ["error", 2],
    "vue/require-default-prop": 0,
    'no-useless-escape': 'off',
    'vue/prop-name-casing': 'off'
  },

  parserOptions: {
    parser: 'babel-eslint'
  },
}
